<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\transaksi;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class univSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $dummyUser = [
            'name' => 'dummy',
            'email' => 'dummy@testing.com',
            'email_verified_at' => Carbon::now(),
            'password' => 'DUMMY',
            'remember_token' => 'adwdawdfawf'
        ];

        $dummyTransac = [
            'id_film' => 23,
            'id_user' => 1,
            'cost' => 900000,
            'status' => true,
        ];

        transaksi::create($dummyTransac);
        User::create($dummyUser);

    }
}
