<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\film;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class filmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $film = [
        [
            'judul' => 'Raya and the Last Dragon',
            'slogan' => 'Art of Disney',
            'sutradara' => 'Carlos Lopez Estrada',
            'poster' => 'https://image.tmdb.org/t/p/original/lPsD10PP4rgUGiGR4CCXA6iY0QQ.jpg',
            'harga' => 480000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'Zack Snyders Justice League',
            'slogan' => '',
            'sutradara' => 'Zack Snyder',
            'poster' => 'https://image.tmdb.org/t/p/original/tnAuB8q5vv7Ax9UAEje5Xi4BXik.jpg',
            'harga' => 500000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'Sentinelle',
            'slogan' => '',
            'sutradara' => 'Julien Leclercq',
            'poster' => 'https://image.tmdb.org/t/p/original/fFRq98cW9lTo6di2o4lK1qUAWaN.jpg',
            'harga' => 312000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'Tom & Jerry',
            'slogan' => 'Mouse & Cat Adventure!',
            'sutradara' => 'Tim Story',
            'poster' => 'https://image.tmdb.org/t/p/original/6KErczPBROQty7QoIsaa6wJYXZi.jpg',
            'harga' => 225000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'Monster Hunter',
            'slogan' => 'When Monsters Comes To Live',
            'sutradara' => 'Paul William Scott Anderson',
            'poster' => 'https://image.tmdb.org/t/p/original/1UCOF11QCw8kcqvce8LKOO6pimh.jpg',
            'harga' => 412000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'Bajocero',
            'slogan' => 'Bertahan Dari Kejamnya Suhu Dingin',
            'sutradara' => 'Lluís Quílez',
            'poster' => 'https://image.tmdb.org/t/p/original/dWSnsAGTfc8U27bWsy2RfwZs0Bs.jpg',
            'harga' => 200000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'Wonder Woman 1984',
            'slogan' => '',
            'sutradara' => 'Patty Jenkins',
            'poster' => 'https://image.tmdb.org/t/p/original/8UlWHLMpgZm9bx6QYh0NFoq67TZ.jpg',
            'harga' => 399000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'Space Sweepers',
            'slogan' => '2092, Space Sweep Begins!',
            'sutradara' => 'Jo Sung-hee',
            'poster' => 'https://image.tmdb.org/t/p/original/qiUesQForGW872kIC0Crqx3vAr0.jpg',
            'harga' => 389000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'Cherry',
            'slogan' => '',
            'sutradara' => 'Joe & Anthony Russo',
            'poster' => 'https://image.tmdb.org/t/p/original/pwDvkDyaHEU9V7cApQhbcSJMG1w.jpg',
            'harga' => 210000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'Coming 2 America',
            'slogan' => '',
            'sutradara' => 'Craig Brewer',
            'poster' => 'https://image.tmdb.org/t/p/original/nWBPLkqNApY5pgrJFMiI9joSI30.jpg',
            'harga' => 201000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'Outside the Wire',
            'slogan' => '',
            'sutradara' => 'Mikael Håfström',
            'poster' => 'https://image.tmdb.org/t/p/original/6XYLiMxHAaCsoyrVo38LBWMw2p8.jpg',
            'harga' => 341000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'Red Dot',
            'slogan' => 'Snipers In Act',
            'sutradara' => 'Alain Darborg',
            'poster' => 'https://image.tmdb.org/t/p/original/xZ2KER2gOHbuHP2GJoODuXDSZCb.jpg',
            'harga' => 489000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'Breach',
            'slogan' => 'Deep In Space They Are Not Alone',
            'sutradara' => 'Billy Ray',
            'poster' => 'https://image.tmdb.org/t/p/original/13B6onhL6FzSN2KaNeQeMML05pS.jpg',
            'harga' => 460000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'The Little Things',
            'slogan' => 'Some Things Never Let Us Go',
            'sutradara' => 'John Lee Hancock',
            'poster' => 'https://image.tmdb.org/t/p/original/c7VlGCCgM9GZivKSzBgzuOVxQn7.jpg',
            'harga' => 399000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'Godzilla vs. Kong',
            'slogan' => 'One Will Fall',
            'sutradara' => 'Adam Wingard',
            'poster' => 'https://image.tmdb.org/t/p/original/pgqgaUx1cJb5oZQQ5v0tNARCeBp.jpg',
            'harga' => 600000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'Pacto de fuga',
            'slogan' => 'No Se Puede Engerrar La Esperanza',
            'sutradara' => 'David Albala',
            'poster' => 'https://image.tmdb.org/t/p/original/qDFfu73R8uO94ydFtdxEdSfTlg6.jpg',
            'harga' => 299000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'Jiu Jitsu',
            'slogan' => '',
            'sutradara' => 'Dimitri Logothetis',
            'poster' => 'https://image.tmdb.org/t/p/original/eLT8Cu357VOwBVTitkmlDEg32Fs.jpg',
            'harga' => 350000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'Black Water: Abyss',
            'slogan' => 'Descend Into Fear',
            'sutradara' => 'Andrew Traucki',
            'poster' => 'https://image.tmdb.org/t/p/original/95S6PinQIvVe4uJAd82a2iGZ0rA.jpg',
            'harga' => 512000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'Miraculous World: New York, United HeroeZ',
            'slogan' => 'Same City Same Duty',
            'sutradara' => 'Thomas Astruc',
            'poster' => 'https://image.tmdb.org/t/p/original/kIHgjAkuzvKBnmdstpBOo4AfZah.jpg',
            'harga' => 569000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'After We Collided',
            'slogan' => 'Is Love Always Like This?',
            'sutradara' => 'Roger Kumble',
            'poster' => 'https://image.tmdb.org/t/p/original/kiX7UYfOpYrMFSAGbI6j1pFkLzQ.jpg',
            'harga' => 199000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'BanG Dream! FILM LIVE',
            'slogan' => 'Girls Band Party!',
            'sutradara' => 'Tomomi Umezu',
            'poster' => 'https://m.media-amazon.com/images/M/MV5BZmQ1MGEyYmQtNDAzYy00YjQ4LTk2MTYtOTUxYmY3ZDQ2YTNiXkEyXkFqcGdeQXVyNzEyMDQ1MDA@._V1_.jpg',
            'harga' => 555000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'judul' => 'Kamen Rider Heisei Generation Forever',
            'slogan' => 'For all who love the Kamen Riders…',
            'sutradara' => 'Kyohei Yamaguchi',
            'poster' => 'https://m.media-amazon.com/images/M/MV5BMmEzMDI1NTItNzI0Ny00MTRhLWFhYzYtOWEyYjZhYWVmMmU2XkEyXkFqcGdeQXVyMTA4NjE0NjEy._V1_.jpg',
            'harga' => 913000,
            'tersedia' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]
        ];

        foreach($film as $Xample){
            film::create($Xample);
        }
    }
}
