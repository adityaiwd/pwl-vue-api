<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\genre;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class genreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $genre = [
            [
                'genre' => 'Action'
            ],
            [
                'genre' => 'Horror'
            ],
            [
                'genre' => 'Drama'
            ],
            [
                'genre' => 'Romance'
            ],
            [
                'genre' => 'Comedy'
            ],
            [
                'genre' => 'Sci-Fi'
            ],
            [
                'genre' => 'Thriller'
            ],
            [
                'genre' => 'Musical'
            ],
            [
                'genre' => 'Documentary'
            ],
            [
                'genre' => 'Adventure'
            ],
            [
                'genre' => 'Fantasy'
            ]
        ];

        foreach($genre as $Xample){
            genre::create($Xample);
        }
    }
}
