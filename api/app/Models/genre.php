<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class genre extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_genre';

    protected $table = 'genre';

    protected $fillable = [
        'genre',
    ];

    public function film()
    {
        return $this->belongsToMany(film::class, 'pivot', 'id_genre', 'id_film');
    }
}
