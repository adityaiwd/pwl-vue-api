<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class film extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_film';

    protected $table = 'film';

    protected $fillable = [
        'judul',
        'slogan',
        'sutradara',
        'poster',
        'harga',
        'tersedia'
    ];

    public function genre()
    {
        return $this->belongsToMany(genre::class, 'pivot', 'id_film', 'id_genre');
    }

    public function transaksi()
    {
        return $this->hasMany(transaksi::class, 'id_film', 'id_film');
    }

    public function cart()
    {
        return $this->hasMany(cart::class, 'id_film', 'id_film');
    }
}
