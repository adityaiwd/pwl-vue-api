<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{
    use HasFactory; 

    protected $primaryKey = 'id_transaksi';

    protected $table = 'transaksi';

    protected $fillable = [
        'id_film',
        'id_user',
        'cost',
        'status',
    ];

    public function film()
    {
        return $this->belongsTo(film::class, 'id_film', 'id_film');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'id_user', 'id');
    }

}
