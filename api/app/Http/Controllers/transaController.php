<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\transaksi;
use App\Models\film;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class transaController extends Controller
{
    //get all transaksi info
    public function index(){
        $result = transaksi::get()->toJson(JSON_PRETTY_PRINT);
        return response($result, 200);
    }

    //get transsaksi by id
    public function transById($id){
        try{
            $result1 = transaksi::findOrFail($id);
            return response($result1, 200);
        }
        catch(ModelNotFoundException $e){
            return response("Tidak ditemukan :(", 404);
        }
    }

    //ga tau nih azzam
    public function set(Request $request)
    {
        $dataFilm = $request->all();
        $id_user = $request->user();

        return response([
           'dataFilm' => $dataFilm, 'dataUser' => $id_user
        ], 200);
    }

    //post traansaski
    public function addTrans(Request $request)
    {
        $user = $request->user();

        $this->validate($request, [
            'id_film' => 'required',
            'id_user' => 'nullable',
            'cost' => 'required',
            'status' => 'required',
        ]);

        $request->merge([
            'id_user' => $user->id
        ]);

        $data = transaksi::create(
            $request->only(['id_film', 'id_user', 'cost', 'status'])
        );

        return response()->json([
            'created' => true,
            'data' => $data
        ], 201);
    }

    //dapetin info transaksi user tsb
    public function getOwnFilm(Request $request)
    {
        $user = $request->user();
        $owned = array();

        try{
            $result1 = transaksi::where('id_user', $user->id)->get();
            
            foreach($result1 as $result){
                $list = film::find($result->id_film);
                array_push($owned, $list);
            }

            $data['owned'] = $owned;
            return response($data, 200);
        }
        catch(ModelNotFoundException $e){
            return response("Tidak ditemukan :(", 404);
        }

    }

    //testing post dengan token
    // public function addTrans(Request $request)
    // {

    // }



}
