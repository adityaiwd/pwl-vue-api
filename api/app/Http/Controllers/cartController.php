<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\cart;
use App\Models\film;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class cartController extends Controller
{
    //dapetin data cart by user
    public function getCart(Request $request)
    {
        $user = $request->user();
        $cart = array();

        try{
            $result1 = cart::where('id_user', $user->id)->get();

            foreach($result1 as $result){
                $id = collect(['id_item' => $result->id_item]);
                $list = film::find($result->id_film);

                $item = $id->merge($list);
                array_push($cart, $item);
            }

            $data['cart'] = $cart;
            return response($data, 200);
        }
        catch(ModelNotFoundException $e){
            return response("Tidak ditemukan :(", 404);
        }
    }

    //add to cart
    public function addCart(Request $request)
    {
        $user = $request->user();

        $this->validate($request, [
            'id_film' => 'required',
            'id_user' => 'nullable',
            'cost' => 'required',
        ]);

        $request->merge([
            'id_user' => $user->id
        ]);

        $data = cart::create(
            $request->only(['id_film', 'id_user', 'cost'])
        );

        return response()->json([
            'created' => true,
            'data' => $data
        ], 201);
    }

    //delete cart
    public function rmCart($id)
    {
        try{
            $item = cart::find($id);
            $item->delete();
            return response("Item dihapus", 200);
        }
        catch(ModelNotFoundException $e){
            return response("Tidak ditemukan :(", 404);
        }

    }

}
