<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\film;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class filmController extends Controller
{

    //get all
    public function index(){
        $result = film::get()->toJson(JSON_PRETTY_PRINT);
        return response($result, 200);
    }

    //ini dapetin info dari id film tsb
    public function filmById($id){
        try{
            $result1 = film::findOrFail($id);
            return response($result1, 200);
        }
        catch(ModelNotFoundException $e){
            return response("Tidak ditemukan :(", 404);
        }
    }

    //ini buat dapetin genre dari id film tsb
    public function getFilmGenre($id){
        try{
            $result1 = film::findOrFail($id)->genre()->get();
            return response($result1, 200);
        }
        catch(ModelNotFoundException $e){
            return response("Tidak ditemukan :(", 404);
        }
    }
}
