<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\film;
use App\Models\genre;
use App\Models\pivot;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class universalController extends Controller
{

    //ini buat search film, masih suka ngebug
    public function search($input)
    {
        $params = explode(' ', $input);
        // $params = $input;

        $result = collect();

        foreach($params as $param)
        {
            // $query = film::with('genre')
            // ->where('judul', 'like', '%'.$param.'%')
            // ->orWhere('slogan', 'like', '%'.$param.'%')
            // ->orWhere('sutradara', 'like', '%'.$param.'%')
            // // ->orWhere('genre', 'like', '%'.$param.'%')
            // ;

            $query = film::whereHas('genre', function($q) use ($param){
                $q->where('genre', 'like', '%'.$param.'%')
                ->orWhere('judul', 'like', '%'.$param.'%')
                ->orWhere('slogan', 'like', '%'.$param.'%')
                ->orWhere('sutradara', 'like', '%'.$param.'%');
            });

            $result = $result->merge($query->get());
            
        }

        $finalResult = $result->unique();

        // foreach($params as $param)
        // {
        //     $query = film::
        //     join('pivot','film.id_film','=','pivot.id_film')->
        //     join('genre','pivot.id_genre','=','genre.id_genre')->
        //     where('judul', 'like', '%'.$param.'%')
        //         ->orWhere('slogan', 'like', '%'.$param.'%')
        //         ->orWhere('sutradara', 'like', '%'.$param.'%')
        //         ->orWhere('genre', 'like', '%'.$param.'%')
        //         ->groupBy('film.id_film');
        // }

        // $query = DB::select("SELECT *
        // FROM film 
        // join pivot on film.id_film=pivot.id_film 
        // join genre on pivot.id_genre=genre.id_genre 
        // where judul 
        // like '%.$params.%' or slogan 
        // like '%.$params.%' or sutradara 
        // like '%.$params.%' or genre 
        // like '%.$params.%' 
        // group by film.id_film");
        
        // $result = $query->get();
        
        if($finalResult->count() == 0){
            return response("Pencarian tidak ditemukan...", 404);
        }

        return response($finalResult, 200);
    }

    //ini buat details, ada genrenya langsung
    public function getFilmDetail($id){

        $genres = array();
        
        try{
            $result2 = film::findOrFail($id);
            $result1 = film::findOrFail($id)->genre()->get();
            foreach($result1 as $result){
                array_push($genres, $result->genre);
            }
            $result2['genre'] = $genres;
            return response($result2, 200);
        }
        catch(ModelNotFoundException $e){
            return response("Tidak ditemukan :(", 404);
        }
    }
}
