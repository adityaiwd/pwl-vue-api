<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\genre;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class genreController extends Controller
{

    //get all genre
    public function index(){
        $result = genre::get()->toJson(JSON_PRETTY_PRINT);
        return response($result, 200);
    }

    //dapetin film2 yg di genre tsb
    public function moviePerGenre($id){
        try{
            $result1 = genre::findOrFail($id)->film()->get();
            return response($result1, 200);
        }
        catch(ModelNotFoundException $e){
            return response("Tidak ditemukan :(", 404);
        }
    }
}
