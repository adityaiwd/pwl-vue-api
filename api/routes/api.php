<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['cors', 'json.response']], function () {
    $baseController =  "App\Http\Controllers";
    Route::post('/login', $baseController.'\Auth\ApiAuthController@login')->name('login.api');
    Route::post('/register',$baseController.'\Auth\ApiAuthController@register')->name('register.api');
    Route::get('/movies', $baseController.'\MovieController@index');
    Route::get('/movies/{id}', $baseController.'\MovieController@show');

    //Film routes
    $baseController =  "App\Http\Controllers";
    Route::get('/film', $baseController.'\filmController@index');
    Route::get('/film/{id}', $baseController.'\filmController@filmById');
    Route::get('/film/genres/{id}', $baseController.'\filmController@getFilmGenre');

    //genre routes
    Route::get('/genre', $baseController.'\genreController@index');
    Route::get('/genre/{id}', $baseController.'\genreController@moviePerGenre');
    
    //transaksi routes
    Route::get('/transaksi', $baseController.'\transaController@index');
    Route::get('/transaksi/{id}', $baseController.'\transaController@transById');

    //universal routes
    Route::get('/search/{input}', $baseController.'\universalController@search');
    Route::get('/film/detail/{id}', $baseController.'\universalController@getFilmDetail');

});

Route::middleware('auth:api')->group(function () {
    $baseController =  "App\Http\Controllers";
    Route::post('/logout', $baseController.'\Auth\ApiAuthController@logout')->name('logout.api');

    //Need Change later
    // Route::post('/transaksi', $baseController. '\transaController@set')->name('trans.api');

    //transaksi
    Route::post('/transaksi', $baseController.'\transaController@addTrans'); 
    Route::get('/owned', $baseController.'\transaController@getOwnFilm'); 
    // Route::get('/transaksitesting', $baseController.'\transaController@testing'); 

    //cart
    Route::post('/cart', $baseController.'\cartController@addCart'); 
    Route::get('/cart', $baseController.'\cartController@getCart'); 
    Route::delete('/cart/{id}', $baseController.'\cartController@rmCart'); 

});
