# PWL Laravel & Vue

## Nama Kelompok 
Aditya Dwi Wicaksono - 185150700111010 <br>
Azzamuddien Hanifa - 185150707111008 <br>
Fathin Al Ghiffari - 185150707111007 <br>
Fauzidan Iqbal Ghiffari - 185150707111003 <br>
Oceandra Audrey - 185150701111007 <br>

## Project setup

<br>

### ***Backend / API / Laravel***

   <hr>

#### **Step 1**
switch to the api directory
```
cd api
```
Install the project with composer
```
composer install
```

#### **Step 2**
Turn on your MySQL database and Setup your .env file corresponding with your own MySQL database, and then do a migration
```
php artisan migrate
```

#### **Step 3**
Install passport to create access client for the access token
```
php artisan passport:install
```

#### **Step 4**
Seed some of the tables
```
php artisan db:seed --class=filmSeeder
php artisan db:seed --class=genreSeeder
php artisan db:seed --class=pivotSeeder
```

#### **Step 5**
Run it on your local server
```
php artisan serve
```
<br>

### ***Frontend / Vue***

   <hr>

#### **Step 1**
switch to the vue directory
```
cd vue
```
Install the project with npm
```
npm install
```

#### **Step 2**
Run it on your local server
```
npm run serve
```
